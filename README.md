# 01-python-planilhas

Algumas planilhas podem ser excessivamente grandes e impossiveis de serem carregadas em editores comuns devido ao número de linhas.

Neste exemplo usaremos uma planilha de dados de importação do ano de 2019 do banco de dados do governo federal.


http://www.mdic.gov.br/index.php/comercio-exterior/estatisticas-de-comercio-exterior/base-de-dados-do-comercio-exterior-brasileiro-arquivos-para-download

Para busca, usaremos o Código 8504 de NCM

NCM 8504
Transformadoreselétricos,conversoreselétricosestáticos(retificadores,por exemplo), bobinas de reatância e de auto-indução.

E calcularemos o valor importado em reais.