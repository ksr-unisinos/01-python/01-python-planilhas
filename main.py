import csv	# leitrura de arquivos CSV
import time	# calculos com tempo

#Nome do Arquivo
filename = 'IMP_2019.csv'

#NCM produrada
ncm = '8504'

#Valor total
total = 0


#função simples de busca
def checkValue(key,value,ncmID):
	size = len(ncmID) 						# guarda o tamanho da lista
	position = key[:size].find(ncmID) 		# verifica se o resultado se encontra em alguma posição
	#position = key.find(ncmID)				# verifica se o resultado se encontra em alguma posição
	if(position == 0): return float(value)	# se se encontra no inicio, retorna o valor da coluna
	
	return 0

inicio = time.time()						# pega o tempo inicial

with open(filename, 'rt') as f:				# abre o arquivo no modo leitura e armazena em 'f'
	reader = csv.reader(f,delimiter=';')	# usa o separador como ';'
	for row in reader: 						# percorre linha por linha
		total += checkValue(row[2],row[10],ncm)	#incrementa no total caso haja match

fim = time.time()							# pega o tempo final
print('Tempo total = ', fim-inicio,'s')		# imprime o delta T


print('R$',total)


nlinhas = sum(1 for line in open(filename))	# calcula o numeo de linhas
print('possui',nlinhas,'linhas')